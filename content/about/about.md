---
title: Sobre este proyecto
slug: about
date: 02 de Abril de 2021
---
Hola, mi nombre es Luis Martínez y soy un desarrollador Fullstack de JavaScript.

Este blog está hecho con [NuxtJS](https://nuxtjs.org/), un framework basado en VueJS. He usado el módulo [$content](https://content.nuxtjs.org/) para agregar contenido al blog mediante archivos `Markdown`. Además esta configurado para usar `SCSS` como lenguaje de estilos.

Haz click en 'Error' en la barra de navegación para ver que ocurre cuando la aplicación no encuentra una página. 😉 


#### ¿Por que Nuxt?

- Permite integrar facilmente el sistema de componentes de Vue JS y me ofrece algunas ventajas adicionales.

- No necesito ningun servidor no ninguna base de datos.

- Facilita mucho el enrutamiento, gracias a su estructura predefinida de carpetas.

- A diferencia de Vue, cuenta con SSR (Server side rendering), lo que permite indexar perfectamente nuestras páginas en los buscadores.

- Facilidad de añadir nuevos posts mediante archivos estáticos markdown.

- Ejecución del contenido dinámico en el lado del cliente. 



Si quieres saber mas visita la documentación de [NuxtJS](https://nuxtjs.org/) y si quieres ver el código de este proyecto haz click [aquí](https://gitlab.com/LuisMarte)

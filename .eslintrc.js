module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
      'space-before-function-paren': ['error', {
      'anonymous': 'never',
      'named': 'never',
      'asyncArrow': 'never'
      }],
	  'indent': [2, 'tab'],
	   'no-tabs': 'off',
	   'vue/html-indent': ['error', 'tab'],
	   "indent": ["error", "tab"]
  }
}
